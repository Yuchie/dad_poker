// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import {NotifyComponent} from "../../Core/view/NotifyComponent";
import {core} from "../../Core/Core";
import {NT} from "../../Constant/constant";

const {ccclass, property} = cc._decorator;

@ccclass
export default class getError extends NotifyComponent {

    @property(cc.Component)
    errorWin: cc.Component = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.errorWin.node.active = false;
    }

    closeWin(){
        this.errorWin.node.active = false;
    }

    notify(data){
        console.log("Error", data);

        this.errorWin.node.active = true;
    }

    // update (dt) {}
}
