// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import {NotifyComponent} from "../../Core/view/NotifyComponent";
import {core} from "../../Core/Core";
import {NT} from "../../Constant/constant";
import Game from "./Game";
import Loading from "../../Loading/Loading";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Notify extends NotifyComponent {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    clickSend(){
        this.sendToServer(core.TYPE.ScratchServer, { 
            key: 'AuthConnect', 
            objKey: 3,
            data: {
                token: '',
                from: 2,           
            }
        });
    }

    start () {

    }

    notify(data){

        console.log("data", data);

       // localStorage.setItem("token", data.token);
        
        if(data.result){
            Loading.obj = true;
            this.getComponent("Loading").check();
            NT.money = data.gold;
        }
    }

    // update (dt) {}
}
