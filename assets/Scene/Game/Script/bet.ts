// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class bet extends cc.Component {

   

    @property(cc.Sprite)
    bet: cc.Sprite[] = [];

    @property(cc.Sprite)
    betCirlce: cc.Sprite = null;

    
    public static betNum = 10;
   
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        
        bet.betNum = 10;

        for(var a = 0; a < this.bet.length; a++){
            if(a == 1){
                
                this.bet[a].getComponent(cc.Sprite).node.color = cc.color(255, 255, 255);
                this.betCirlce.node.position = this.bet[a].getComponent(cc.Sprite).node.position;
            }else{
                
                this.bet[a].getComponent(cc.Sprite).node.color = cc.color(64,64,64);
            }
          //  this.bet[a].getComponent(cc.Sprite).node.color = cc.color(128,138,135);
        }

       
    }

    


    reset(){
        bet.betNum = 10;

        for(var a = 0; a < this.bet.length; a++){
            if(a == 1){
                
                this.bet[a].getComponent(cc.Sprite).node.color = cc.color(255, 255, 255);
                this.betCirlce.node.position = this.bet[a].getComponent(cc.Sprite).node.position;
            }else{
                
                this.bet[a].getComponent(cc.Sprite).node.color = cc.color(64,64,64);
            }
          //  this.bet[a].getComponent(cc.Sprite).node.color = cc.color(128,138,135);
        }
        
    }

  

    click(e, b){

        if(parseInt(b) == 0){
            bet.betNum = 3;
        }else if(parseInt(b) == 1){
            bet.betNum = 10;
        }else if(parseInt(b) == 2){
            bet.betNum = 100;

        }

        for(var a = 0; a < this.bet.length; a++){

            if(parseInt(b) == a){
                
                this.bet[parseInt(b)].getComponent(cc.Sprite).node.color = cc.color(255, 255, 255);
                this.betCirlce.node.position = this.bet[parseInt(b)].getComponent(cc.Sprite).node.position;

            }else{
                
                this.bet[a].getComponent(cc.Sprite).node.color = cc.color(64,64,64);
            }
        }
    }

    // update (dt) {}
}
