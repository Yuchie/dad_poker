// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class animateMoney extends cc.Component {

    @property(Number)
    IncreaseGap: number = 0.001;

    private spriteNumber: cc.Sprite[] = [];
    private frameNumber: cc.SpriteFrame[] = [];

    private setUpNum: number;
    private nowNum: number;
    private nowNumStr: string = "";
    private checkLength: string = "";
    private run = false;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.getNumberTexture();
    }

    start () {
        this.enabled = false;
    }

    setWinMoney(num: number, x:number, y: number){
        this.checkLength = "" + num.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');

        this.spriteNumber.length = this.checkLength.length;

        this.setUpSprit(x, y);
        
        this.enabled = true;

        this.nowNum = 0;

        this.setUpNum = num;

        this.IncreaseGap = num / (10 * 3);

        if (this.IncreaseGap < 0.0001) {
            this.IncreaseGap = 0.0001;
        }
    }

    reset(){
        this.enabled = false;

        this.nowNum = 0;

        for (var a = 0, len = this.spriteNumber.length; a < len; a++) {
            this.spriteNumber[a].spriteFrame = this.frameNumber[0];
        }
    }

    setRunMoneySpritFrame(){
        let str = this.nowNumStr.split("").join("");

        for (var a = 0, b = 0, len = str.length; a < len; a++) {
            if (str[a] !== '.' && str[a] !== ',') {
                this.spriteNumber[b].spriteFrame = this.frameNumber[parseInt(str[a])];
                this.spriteNumber[b].node.scale = 1.3;
            }else if(str[a] == ','){
                this.spriteNumber[b].spriteFrame = this.frameNumber[10];
                this.spriteNumber[b].node.scale = 1.3;
            }else {
                this.spriteNumber[b].spriteFrame = this.frameNumber[11];
                this.spriteNumber[b].node.scale = 1.3;
            }
            b++;
        }
    }

    setMoneySpritFrame(){
        let str = this.checkLength.split("").join("");

        for (var a = 0, b = 0, len = str.length; a < len; a++) {
            if (str[a] !== '.' && str[a] !== ',') {
                this.spriteNumber[b].spriteFrame = this.frameNumber[parseInt(str[a])];
                this.spriteNumber[b].node.scale = 1.3;
            }else if(str[a] == ','){
                this.spriteNumber[b].spriteFrame = this.frameNumber[10];
                this.spriteNumber[b].node.scale = 1.3;
            }else {
                this.spriteNumber[b].spriteFrame = this.frameNumber[11];
                this.spriteNumber[b].node.scale = 1.3;
            }
            b++;
        }
    }

    runMoneyEnd(){
        if(this.run == false && this.enabled == false){
            for (var a = 0, len = this.spriteNumber.length; a < len; a++) {
                this.spriteNumber[a].node.destroy();
            }
        }
    }

    runMoney() {
        this.enabled = true;
        this.run = true;
        console.log("run");
    }

    update (dt) {
        if (this.run){

            this.nowNum = parseFloat((this.nowNum + this.IncreaseGap).toFixed(2));

            if (this.nowNum >= this.setUpNum){
                var te = (Math.floor(this.setUpNum * 10000 + 0.000000001) / 10000) + 0.00000001;
                var tearr = (te + "").split('');
                this.nowNumStr = "";
                var spotIndex = tearr.indexOf('.');
                for(var a = 0, b = 0; a <= tearr.length; a++){
                    if(a > spotIndex){
                        this.nowNumStr += tearr[a];
                        b++;
                        if(b >= 4){
                            break;
                        }else{
                            this.nowNumStr += tearr[a];
                        }
                    }
                }

                this.setMoneySpritFrame();
                this.run = false;
                this.enabled = false;

                // var obj = setTimeout(()=>{
                //     this.runMoneyEnd();
                // }, 6000);
            }else{
                this.nowNumStr = this.nowNum.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                this.setRunMoneySpritFrame();
            }
        }
    }

    private getNumberTexture(){
        for (var a = 0; a < 13; a++){
            var name: any = a;
            if (a >= 10) {
                switch (a) {
                    case 10:
                        name = "comma";
                        break;
                    case 11:
                        name = "point";
                        break;
                    case 12:
                        name = "x";
                        break;
                    default:
                        break;
                }
            }

            var deps = cc.loader.getDependsRecursively("Sprite/num/" + name);

            for (var i = 0; i < deps.length; ++i) {
                var item = cc.loader.getRes(deps[i]);

                if (item instanceof cc.Texture2D) {
                    var spFr = new cc.SpriteFrame();
                    spFr.setTexture(item);
                    this.frameNumber.push(spFr);
                }
            }
        }
    }

    private setUpSprit(x:number, y:number) {
        let initX = x;
        let initY = y;

        for (var a = 0, len = this.spriteNumber.length; a < len; a++) {
            var node = new cc.Node();

            this.spriteNumber[a] = node.addComponent(cc.Sprite);

            this.node.addChild(node);
            
            if (a == 3 || a == 7) {
                this.spriteNumber[a].node.x = initX + (a * 110) + 70;
            }

            this.spriteNumber[a].node.x = initX + (a * 110) - 20;
            this.spriteNumber[a].node.y = initY - 50;
        }
    }
}
