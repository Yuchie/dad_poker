// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class zoomInOut extends cc.Component {

    @property
    zoomInSize: number = 0.8;

    @property
    zoomInTime : number = 200;

    private isTap : boolean = false;
    private tTime : number = 0;
    private spStr : string = "";
    private isSpin : boolean = false;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.enabled = false;
    }

    setIsSpin(b:boolean){
        this.isSpin = b;
    }

    objTap (e , spStr){
        if(this.isSpin){
            return;
        }

        this.tTime = 1;
        this.enabled = true;
        this.isTap = true;
        this.spStr = spStr;
    }

    update (dt) {
        if(this.isTap && this.tTime <= this.zoomInTime){

            this.tTime += dt*1000;

            var psent = this.tTime/this.zoomInTime;

            if(psent > 1){
                this.node.scale = 1;
                this.isTap = false;
                this.enabled = false;
                if(this.spStr == 'hide'){
                    this.node.active = false;
                }
            }
            else{
                this.node.scale = this.zoomInSize + this.zoomInSize*(1- psent);
            }
        }
        else{
            this.tTime = 0;
        }
    }
}
