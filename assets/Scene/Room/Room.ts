// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Room extends cc.Component {

    @property(cc.ScrollView)
    public scrollView: cc.ScrollView = null;

    @property(cc.Node)
    public itemTemplate: cc.Node = null;
    
    private spawnCount= 9;

    private totalCount= 300;

    private spacing = 10;

    private bufferZone= 50;

    private lastContentPosY = 0;

    private content = null;

    private items = [];
    // LIFE-CYCLE CALLBACKS:

     onLoad () {
        this.content = this.scrollView.content;
        this.items = [];
        this.lastContentPosY = 0;                   //用来保存content在Y坐标的值
        //根据循环需要的数量调整content的高度
        this.content.height = this.totalCount * (this.itemTemplate.height + this.spacing) + this.spacing; 
        

        
        
        //创建必要的Item
        var count = 0;
       // for (let i = 0; i < (this.spawnCount/20); ++i) { 
            
            for(var a = 0; a < 5; a++){
                for(var b = 0; b < 4; b++){
                    count++;
                    var item = this.getNewSprite("desktop"+count, "Sprite/desktop");
                    this.content.addChild(item);
                    console.log(count);
                    //第一个item的位置是     - (间隔高度 + 一半高度的item);  后面以此减去间隔,和一item的高度
                    item.setScale(0.18);
                    // if((count-1) % 4 == 0){
                    //     item.setPosition(-70+(b*item.width), -100 -(item.height*a));
                    // }else{
                    item.setPosition(-80+(b*55), -110 -(35*a));
                   // }
                    
                    
                    this.items.push(item);



                }
            }
            
            
        //}


     }



    start () {
        

    }

    getPositionInView (item) { //位置转换到ScrollVeiw的坐标系,判断是否出了可视范围
        let worldPos = item.parent.convertToWorldSpaceAR(item.position);
        let viewPos = this.scrollView.node.convertToNodeSpaceAR(worldPos);
        return viewPos;
    }

    update (dt){
        let items = this.items;
        let buffer = this.bufferZone;
        let isDown = this.scrollView.content.y < this.lastContentPosY; 
        let offset = (this.itemTemplate.height + this.spacing) * items.length;
        for (let i = 0; i < items.length; ++i) {
            let viewPos = this.getPositionInView(items[i]);
            if (isDown) {
                // if away from buffer zone and not reaching top of content
                if (viewPos.y < -buffer && items[i].y + offset < 0) {
                    items[i].y = items[i].y + offset;
                    let item = items[i].getComponent('Item');
                    let itemId = item.itemID - items.length; // update item id
                    item.updateItem(i, itemId);
                }
            } else {
                if (viewPos.y > buffer && items[i].y - offset > -this.content.height) {
                    items[i].y = items[i].y - offset;
                    let item = items[i].getComponent('Item');
                    let itemId = item.itemID + items.length;
                    item.updateItem(i, itemId);
                }
            }
        }
      
        this.lastContentPosY = this.scrollView.content.y;



    }
    
        


        

    

    getNewSprite(nodeName : string ,url: string ):cc.Node {
        var node = new cc.Node(nodeName);
        var sprite = node.addComponent(cc.Sprite);
        var deps = cc.loader.getDependsRecursively(url);
        var textures =[];

        for (var i = 0; i < deps.length; ++i) {
            var item = cc.loader.getRes(deps[i]);

            if (item instanceof cc.Texture2D) {
                var spFr = new cc.SpriteFrame();
                spFr.setTexture(item);
                sprite.spriteFrame = spFr;
            }
        }
        return node;
    }

    // update (dt) {}
}
