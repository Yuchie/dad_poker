
import {Message} from "./Message";

export  class  MsgToJson implements Message {

    public static  msgToJson :MsgToJson;

    init(){
        
    }
    encdoe ( msg : Object) {

        return  JSON.stringify( msg );
    }

    decode( msg : string){

        return JSON.parse( msg );
    }

    public static getInstance() :MsgToJson{

        if(MsgToJson.msgToJson === null || MsgToJson.msgToJson ===undefined ){

            MsgToJson.msgToJson = new MsgToJson();
        }

        return MsgToJson.msgToJson;

    }
    
}
