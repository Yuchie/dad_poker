
export interface Connect{
    
    init(url:string ,isSSH?:boolean);
    send(method :any ,  param ?: any , header?:any );
    send(msg : any);
    close();
    addEventListener(type:number , fn: Function, bindObj:Object):void;
}