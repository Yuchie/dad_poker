import {Connect} from "./Connect";

export class HTTPConnect implements Connect {


    constructor() {
        
    }

    private xhr : XMLHttpRequest = null;
    private rootUrl:string="";

    private callback : Function = null;
    private bindObj : Object = null;



    public init (url: string, isSSH?: boolean){
        this.xhr = new XMLHttpRequest();

        this.rootUrl = url;
        this.xhr.onreadystatechange=  this.readyStateChange;

    }

    public send( method:string , param ?: Object , header?:Object){

        var paramstr =[];

        for(var k in param ){
            
            paramstr.push(k+"="+param[k]);

        }
        var sendStr = paramstr.join("&");
        if(paramstr.length >0){
            sendStr ="?"+sendStr;
        }

        this.xhr.open("GET" ,this.rootUrl+method+sendStr , true );

        if(header){

            for(var k in header){

                this.xhr.setRequestHeader(k , header[k]);
            }
            
        }
    
        this.xhr.onreadystatechange=  this.readyStateChange.bind(this);
        this.xhr.send();
    }

    public addEventListener(){

    }

    public readyStateChange( ){
       
        if (this.xhr.readyState == 4 && (this.xhr.status >= 200 && this.xhr.status < 400)) {
            var response = this.xhr.responseText;
           
            this.callback.call( this.bindObj ,response );
        }
    }

    public setCallBack (callback , bindObj){

        this.callback = callback;
        this.bindObj = bindObj;
    }

    public close(){}

}