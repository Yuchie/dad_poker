/**
 * CmdInvoker
 */
import {NETWORK} from "./../constant/core_constant";
import {Command} from "./Command";

export class CmdInvoker {

    private cmds : Command[];

    constructor() {
        this.cmds = [];
        for(var k in NETWORK.STATE ){
            this.cmds.push(null);
        }
    }

    public clear(){
        for(var a=0,len =this.cmds.length; a<len; a++ ){
            if(this.cmds[a]){
                
                this.cmds[a].clear();
                this.cmds[a] = null;
            }
        }
    }

    public storeCmd(   cmd : Command ): void{
        this.cmds[ cmd.getState() ] = cmd;
    }

    public exeCmd( state: number ,  data: any ){
        
        this.cmds[state].execute(data);
    }
}