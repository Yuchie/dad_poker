import {Controller} from './controller/Controller';
import {TYPE} from './model/TypeSet';

//import SERVERREF from './controller/Controller';

export class core{
    public static _core:core;
    public static  TYPE :any;
    
    constructor(){
        Controller.getInstance();
        core.TYPE = TYPE;
    }

    getCtrl():Controller{
        return Controller.getInstance();
    }

    connectTo(serverType: number, connectState: number, closeFn?: Function){
        Controller.getInstance().startServerConnect(serverType, connectState);
    }

    registView(state, view){
        Controller.getInstance().registView(state, view);
    }

    send(serverType: number, param: any){
        Controller.getInstance().send(serverType, param);
    }

    close (serverType: number){
        Controller.getInstance().close(serverType);
    }

    get TYPE():TYPE{
        return core.TYPE;
    }

    public static getInstance():core{
        if(core._core === null || core._core === undefined){
            core._core = new core();
        }
        
        return core._core;
    }
}