
import {NT} from "./../../constant/constant";

var errorMsg = "";
if(!NT){
    errorMsg += ("constant class name shoud be NT  \n");
    if(!NT.Config){

    }
}
if(NT){
    if(!NT.Config){
        errorMsg += ("NT Shoud content the Config static parameter  \n");
    }
    if(!NT.STATE){
        errorMsg += ("NT Shoud content the STATE static parameter  \n");
    }
    
}

if(errorMsg!==""){
    throw( errorMsg);
}

export class NETWORK  extends NT {
    
    public static network : NETWORK;
    public static MSGTYPE  = NT.MSGTYPE;
    public static STATE = NT.STATE;
    public static config:{TYPE:string , HOST:string , PORT: number}[] = NT.Config;

    constructor(){
        super();
       
    }

    public static getInstance():NETWORK{

        if(NETWORK.network===null || NETWORK.network===undefined ){

            NETWORK.network =new NETWORK();
        }

        return NETWORK.network;
    }

}
