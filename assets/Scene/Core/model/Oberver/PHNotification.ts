
import {Subject} from './Subject';

export  class PHNotification extends Subject{
    private subjectState : number ;
    
    public static instance: PHNotification;
    
    constructor(){
        super();
    
    }
    
    get state(): number{
        
        return this.subjectState;
    }
    
    set state(s: number ) {
        
       
        this.subjectState = s;
    }
    
    static getInstance(): PHNotification{
        
        if(PHNotification.instance===null || PHNotification.instance === undefined){
            PHNotification.instance = new PHNotification();
            
        }
        
        return PHNotification.instance;
    }

    
}