
import {Connect} from "../lib/Connect/Connect";
import {NetWork} from '../Adapter/WebsocketAdopter';
import {MsgProto} from "../lib/Msg/MsgProto";
import {MessageAdopter} from "../Adapter/MessageAdopter";

import { NETWORK } from "../constant/core_constant";
import {PHNotification} from "./Oberver/PHNotification";

export class DataModel {

    private serverType:string = null;

    private messageAdopter:any;

    private wsConnect :Connect = null;

    private static instance: DataModel;
    
    private STATE = -1;

    private host: string ;
    private port : number;
    private timeoutObj ;
    private timeoutNoRespObj ;
    private isShow: boolean  = false;
    private isLocal :boolean = false;
    private isHttp: boolean = false;
    private isWorker : boolean = false;
    private isSSL : boolean = false;

    private checkReady : any = null;

	public constructor( serverType: string , host:string , port:number , initState?: number , closeFn?: Function , bkList?:any , isSSL?:boolean) {
       
        this.isShow = false;

        this.serverType = serverType;

        if( initState  ){
            this.STATE = initState;
            
        }
        if(serverType =="WORKER"){
            this.isWorker = true;
            // this.wsConnect  = wx.createWorker("roomWorker/index.js");
            // this.wsConnect.onMessage(this.onWorkerMessage.bind(this));
        }
        else if(serverType == "HTTP"){
             this.host = host;
             this.port = port;
             this.isHttp = true;
        }
        else if(serverType !== "LOCAL"){
            
            
            this.messageAdopter = MessageAdopter.getDecodeMsg(NETWORK.MSGTYPE ,null);
          
            this.wsConnect = NetWork.getConnect(NetWork.SOCKETTYPE.WEBSOCKET);
            this.isSSL = isSSL;
            this.host = host;
            this.port = port;

            // this.wsConnect.init(this.host+":"+this.port , false);

            // this.wsConnect.addEventListener(NetWork.EVENT.OPEN , this.onopen , this  );
            // this.wsConnect.addEventListener(NetWork.EVENT.ONMESSAGE , this.onWSMessage , this  ); 


            // if(closeFn){
            //     this.wsConnect.addEventListener( NetWork.EVENT.CLOSE, closeFn, this); 
            //     this.wsConnect.addEventListener( NetWork.EVENT.ERROR , closeFn, this);  
            // }
            // else{
            //     this.wsConnect.addEventListener( NetWork.EVENT.CLOSE, this.onclose, this); 
            //     this.wsConnect.addEventListener( NetWork.EVENT.ERROR, this.error, this);  
            // }

            // this.timeoutObj = new egret.Timer(CLOSETIME.TIMEOUT*1000 );
            

            // this.timeoutNoRespObj = new egret.Timer( CLOSETIME.NORESP*1000 );
        }
        else{
            this.isLocal = true;
        }
        
	}

    

    // public runTimeout(){
       
    //     var dataObj = ModelFactory.getCloseObj( );
    //     dataObj.setData({closeState:CLOSTSTATE.TIMEOUT});
    //     if(!this.isShow){
    //         PHNotification.getInstance().notify(dataObj);
    //     }
    //     this.isShow = true;
    //     // this.timeoutObj.stop();
    //     // this.timeoutObj.removeEventListener(egret.TimerEvent.TIMER, this.runTimeout, this );
    //     // this.timeoutObj = null;
    //     this.wsConnect.close();
    // }
    // public runTimeoutNoResp(){
        
    //     var dataObj = ModelFactory.getCloseObj( );
    //     dataObj.setData({closeState:CLOSTSTATE.NORESP});
    //     if(!this.isShow){
    //         PHNotification.getInstance().notify(dataObj);
    //     }
    //     this.isShow = true;
    //     this.timeoutNoRespObj.stop();
    //     this.timeoutNoRespObj.removeEventListener(egret.TimerEvent.TIMER, this.runTimeoutNoResp, this );
    //     this.timeoutNoRespObj = null;
    //     this.wsConnect.close();
    // }

    // public block( block:boolean ){
        
    //     this.messageAdopter.setBlock(block);
    // }

    public connect():void{
        // if(this.isLocal || this.isHttp){
        //     return ;
        // }
        // this.isShow = false;
        
        // var prefix = "ws";
        
        // if(connectSSL){

        //     prefix = "wss";
        // }

        //  var url = prefix+ "://"+this.host;
        // if(!isPath){
        //     url =prefix +"://"+this.host +":"+this.port;
        // }
        
        // this.wsConnect.connectByUrl( url )//+":"+this.port );
        
        if(this.checkReady){

            clearInterval(this.checkReady);
        }


        if(MsgProto.readyDecode) {

            this.wsConnect.addEventListener(NetWork.EVENT.OPEN , this.onopen , this  );
            this.wsConnect.addEventListener(NetWork.EVENT.ONMESSAGE , this.onWSMessage , this  ); 
            
            this.wsConnect.addEventListener( NetWork.EVENT.CLOSE, this.onclose, this); 
            this.wsConnect.addEventListener( NetWork.EVENT.ERROR, this.error, this); 

           // this.wsConnect.init(this.host+":"+this.port , this.isSSL);
           this.wsConnect.init(this.host , this.isSSL);
        }
        else {

            this.checkReady = setTimeout(()=>{

                if(this.checkReady){

                    clearInterval(this.checkReady);
                }
                
                this.connect();
                
            }, 1000)
        }

        

        

        // if(closeFn){
        //     this.wsConnect.addEventListener( NetWork.EVENT.CLOSE, closeFn, this); 
        //     this.wsConnect.addEventListener( NetWork.EVENT.ERROR , closeFn, this);  
        // }
        // else{
        //     this.wsConnect.addEventListener( NetWork.EVENT.CLOSE, this.onclose, this); 
        //     this.wsConnect.addEventListener( NetWork.EVENT.ERROR, this.error, this);  
        // }
    }
    public sendToServer(data){

        var msgData = this.messageAdopter.encdoe( data );
        
        try{
            this.wsConnect.send( msgData );
        }catch(e){
            this.error();
        }
        

    }

    
    public send(state : number, data: any , isLocal : boolean , httpHeader?:any  ):void{
        // if(this.isWorker ){
        //     if(state){
        //         // var model = ModelFactory.getSendFactory(state,data);
            
        //         // this.wsConnect.postMessage( model.getSendData());
                
        //     }
        //     else{
                
        //     }
        // }
        // else if(isLocal){
        
        //     if(data.state===null || data.state===undefined ){
        //         data.state = state;
        //     }

        //     this.onLocalMessage( data );
        // }
        // else if( this.serverType =="HTTP"){  
            
        //     var model = ModelFactory.getSendFactory(state, data);
            
        //     this.httpSend(state , APIREF[state] ,  model.getSendData() ,httpHeader);

        // }
        // else{
        //     // if(this.timeoutObj){
        //     //     this.timeoutObj.stop();
        //     // }
            
        //     // if(this.timeoutNoRespObj){
        //     //      this.timeoutNoRespObj.stop()
        //     // }
           
        //     if(state){
        //         var model = ModelFactory.getSendFactory(state,data);
            
        //         var msgData = this.messageAdopter.setMsg( model.type ,  model.getSendData() );
                
        //         var bytes =  new egret.ByteArray(msgData);

        //         this.wsConnect.writeBytes( bytes );
        //     }
        //     else{
                
        //     }
            
        // }
        
	}

    public error(){
        console.log("socker error")
        PHNotification.getInstance().state = 12;
        PHNotification.getInstance().notify({key : 12 , close : true});
    }

    public close(){
        console.log("onclose herer ")
        this.wsConnect.close();
        // if( this.timeoutObj){
        //     this.timeoutObj.stop();
        //     this.timeoutObj.removeEventListener(egret.TimerEvent.TIMER, this.runTimeoutNoResp, this );
        //     this.timeoutObj = null;
        // }
        // if(this.timeoutNoRespObj){
        //     this.timeoutNoRespObj.stop();
        //     this.timeoutNoRespObj.removeEventListener(egret.TimerEvent.TIMER, this.runTimeoutNoResp, this );
        //     this.timeoutNoRespObj = null;
        // }
        //this.wsConnect.close();
        

    }
	
    public onopen(obj: any): void {
        
    }
    
    public onWSMessage(e): void {
        
        var  data =this.messageAdopter.decode(e);

        PHNotification.getInstance().state = data.key;
        PHNotification.getInstance().notify(data);
        // if( this.serverType == null ){

        //     throw("Server Type need setup");
        // }
    
        //this.timeoutNoRespObj.stop();

        // var byte:egret.ByteArray = new egret.ByteArray();
        
        // this.wsConnect.readBytes(byte);

        
        // var objDecode = this.messageAdopter.getMsg( this.serverType, byte.buffer);

        // if( !objDecode){
        //     return ;
        // }
        
        // var dataObj = ModelFactory.getFactory( objDecode.toString(),  objDecode , false);
        
        
        // if(dataObj!== null ){
           

        //     PHNotification.getInstance().state = dataObj.getState();

        //     PHNotification.getInstance().notify(dataObj);

            
        // }
    
    }

    public onLocalMessage(obj: any): void {
        
        //var dataObj = ModelFactory.getLocalFactory( obj.state ,  obj);
        // var dataObj = ModelFactory.getFactory( obj.state ,  obj , true);
      
        // PHNotification.getInstance().state = dataObj.getState();

        // PHNotification.getInstance().notify(dataObj);
    }

    

    public onclose(obj: any): void {

        console.log('on close');
        PHNotification.getInstance().state = 999;
        PHNotification.getInstance().notify({key : 999 , close : true});
        // if( this.timeoutObj){
        //     this.timeoutObj.stop();
        //     this.timeoutObj.removeEventListener(egret.TimerEvent.TIMER, this.runTimeoutNoResp, this );
        //     this.timeoutObj = null;
        // }
        // if(this.timeoutNoRespObj){
        //     this.timeoutNoRespObj.stop();
        //     this.timeoutNoRespObj.removeEventListener(egret.TimerEvent.TIMER, this.runTimeoutNoResp, this );
        //     this.timeoutNoRespObj = null;
        // }
        
        // var dataObj = new DefaultCloseModel(this.serverType );
        // dataObj.setData({closeState:CLOSTSTATE.SERVERCLOSE});
      
        // PHNotification.getInstance().notify(dataObj);

        // Controller.getInstance().removeConn(this.serverType);
    }    
}
