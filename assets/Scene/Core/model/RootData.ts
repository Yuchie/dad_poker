/**
 *
 * @author 
 *
 */
import {ModelFactory} from "./ModelFactory";
export  class RootData {
    
    public type: string = "";
    public obj: string = "";
    public objKey : number =0;
    public data: any;
    public resp: boolean;
    public respType: number;
    public state : number ;
    public respObj: string;
    public respData: any;
    public static rootData: RootData;

	public constructor() {
      
	}   

    
    setRetrunData( data: any): void{
 
            this.respData = data;
    }

	setData( data : any ): void {
        //throw("You shoud override the function");
        // console.log(data);
        this.setRetrunData(  data );
    }

    getSendDataTemp(): DataInterface {
        return {
            type: this.type,
            obj: this.obj,
            objKey : this.objKey,
            data: this.data,
            resp: true,
            state: this.state,
            respType: this.respType
        };
    }

    getState():number{
       
       return this.state;
    }
    
    getSendData():any{

        return this.data;
    }
    public static getInstance():RootData{
       
        if(this.rootData ===null || this.rootData===undefined ){
       
            this.rootData = new this;

            ModelFactory.getInstance().reigstModel(
                this.rootData.type , 
                this.rootData.obj ,
                this.rootData  , 
                this.rootData.state , 
                this.rootData.respType , 
                this.rootData.respObj );
        }
        return this.rootData;
    }
   
}