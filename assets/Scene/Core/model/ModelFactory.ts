import {RootData} from "./RootData";

/**
 *
 * @author 
 * @version v2.0.0
 *
 */
var MSG = {};

export class ModelFactory {
   
    public modelObjs:any[];
    private outKey : string[];
    private static closeModelObj : RootData;
    private static modelFactory: ModelFactory;
    public modelRespType: number[]=[];
    public modelState : number[] = [];

	public constructor() {

        this.modelObjs = [];

        this.outKey = [];

        this.registCloseObj ( );

	}


    public reigstModel( serverType: string  ,  key: string  , modelObj : RootData ,state :number , respType: number ,respObj: string ){
        
        this.modelObjs.push( { type:serverType , obj: key , model: modelObj , state: state ,respType:respType }  );
        this.modelRespType[respType] = this.modelObjs.length-1;
        this.modelState[state] = this.modelObjs.length-1;
     
        MSG[respType] =respObj;
    
    }

    public unregistModel(serverType: string  , key:string ){

        var result = false;

        var len =this.modelObjs.length;

        for(var k=0 ; k< len;  k+=1  ) {

            if( this.modelObjs[k].type == serverType && this.modelObjs[k].obj == key ){

                this.modelRespType[ this.modelObjs[k].respType ] = null;

                this.modelState[ this.modelObjs[k].state ] = null;

                this.modelObjs.splice(k , 1);
                
                result = true;

                break;
                
            }
        }

        return result;
    }
	
    public static getInstance ():ModelFactory{
        
        if( ModelFactory.modelFactory===null || ModelFactory.modelFactory===undefined ){

             ModelFactory.modelFactory = new ModelFactory();
        }

        return ModelFactory.modelFactory;
    }

	public static getFactory( dataName : string , data :any , isLocal :boolean ) :RootData{
        let model : any;
        var modObj =  ModelFactory.getInstance().modelObjs;
        var modState = ModelFactory.getInstance().modelState;
        var modRespType = ModelFactory.getInstance().modelRespType;

    
        if(isLocal){
            
            let dataNameInt = parseInt(dataName);
            
            if(modObj[ modState[dataNameInt] ].model  ){
                
                modObj[ modState[dataNameInt]].model.setData(data);

                return modObj[ modState[dataNameInt]].model;
            }
            else{

                console.warn("Model get Error , dataName:" +dataName+ ", isLocal:"+isLocal );
            }
        }
        else{
            // console.log(data);
             if( modObj[ modRespType[data.key] ].model ){
                //  console.log(data);
                modObj[ modRespType[data.key]].model.setData(data);

                return modObj[ modRespType[data.key]].model;
               
            }
            else{
                console.warn("Model get Error , dataName:" +dataName+ ", isLocal:"+isLocal );
            }
           
        }

	}
	
    public static getSendFactory( state : number , data : any): RootData{
        
        let model : any;
                        
        var modObj  = ModelFactory.getInstance().modelObjs
                        
         for(var k=0,len= modObj.length; k < len ;k++ ){
                if(  state  == modObj[k].state   ) {
                      
                        modObj[k].model.setSendData(data);
                        return modObj[k].model ;
                }
            }
       
        return model;
	}

    public registCloseObj ( ){
        ModelFactory.closeModelObj=  DefaultCloseModel.getInstance();;
    }

    public static  getCloseObj () : RootData{
    
        return ModelFactory.closeModelObj;
    }
}