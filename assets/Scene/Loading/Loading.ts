// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import {NotifyComponent} from "../Core/view/NotifyComponent";
import {core} from "../Core/Core";
import {NT} from "../Constant/constant";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Loading extends NotifyComponent {



    @property(cc.Label)
    public progressLabel: cc.Label = null;

    @property(cc.Label)
    public progressLabel1: cc.Label = null;

    @property(cc.Label)
    public progressLabel2: cc.Label = null;

    


    public static obj: boolean = false;
    private isComplete: boolean = false;

    private num1 :string =  "";
    private num2 :string =  "";

    private num3 : string = "";
   // private getToken;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        //this.startBtn.node.active = false;
        
        core.getInstance().connectTo(core.TYPE.SCRATCH, 1);
        
        cc.loader.loadResDir("Sprite", this.onprocess.bind(this), this.oncomplete.bind(this));
    }


    onprocess(completeCount, totalCount, item){
        // this.clean();

        var persent = Math.floor((completeCount / totalCount)*100);
        var s = completeCount / totalCount;
        var persentString = ""+persent;
        var str = persentString.split("");
        var length = str.length;
        

      
        if(length == 3){

        if(str[2] != undefined){
            this.progressLabel.string = str[2];

        }
        
        if(str[1] != undefined){
            this.progressLabel1.string = str[1];
        }
        
        if(str[0] != undefined){
            this.progressLabel2.string = str[0];
        }

        }else if(length == 2){
            
            if(str[1] != undefined){
                this.progressLabel.string = str[1];
    
            }
            
            if(str[0] != undefined){
                this.progressLabel1.string = str[0];
            }
            
           

        }else{
            if(str[0] != undefined){
                this.progressLabel.string = str[0];
    
            }

        }


       

        

    }
    

    oncomplete (err, spriteFrame){
        //console.log(this.node);
        this.isComplete = true;
        this.check();
     
    }

    check(){
      //  if(this.isComplete && Loading.obj){
            cc.director.loadScene("Room");

      //  }
       

    }
    getNewSprite(nodeName : string ,url: string ):cc.Node {
        var node = new cc.Node(nodeName);
        var sprite = node.addComponent(cc.Sprite);
        var deps = cc.loader.getDependsRecursively(url);
        var textures =[];

        for (var i = 0; i < deps.length; ++i) {
            var item = cc.loader.getRes(deps[i]);

            if (item instanceof cc.Texture2D) {
                var spFr = new cc.SpriteFrame();
                spFr.setTexture(item);
                sprite.spriteFrame = spFr;
            }
        }
        return node;
    }

    notify(data){
      
        console.log("connect",data);
        // if(localStorage.getItem("token") == null){
        //     this.getToken = "";
        // }else{
        //     this.getToken = localStorage.getItem("token");
        // }
        var ran = Math.floor(Math.random()* 100000);
       // if(data.result){

           // cc.director.loadScene("Game");
            this.sendToServer(core.TYPE.SCRATCH ,{ 
                key: 'AuthConnect', 
                objKey: 2,
                data: {
                    account: "test",
                    pwd: "test",
                    vendor: "test",
                    msgId: 0,
                
                    
                    // token: this.getToken,
                    // from: NT.from,
                }
            });
       // }
    }

    // update (dt) {}
}
