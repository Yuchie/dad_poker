export class NT{
    public static protoUrl : string[] = ["./proto/Auth", "./proto/Error", "./proto/Game", "./proto/User"];
    public static protoName : string[] =["Auth", "Error", "Game", "User"];
    //0 : json , 1 : protobuf , 2 :json 
    public static MSGTYPE  = 1;

    public static STATE = {
      
        Connect: 1,
        AuthConnect: 2,
        AuthConnectRes: 3,
        GetBetRange: 51,
        GetBetRangeRes: 52,
        BetRange: 53,
        SetBet: 54,
        SetBetRes: 55,
        GetMachineList: 56,
        GetMachineListRes: 57,
        GetMachineDetial: 58,
        IntoGame: 60,
        IntoGameRes: 61,
        Bet: 62,
        BetRes: 63,
        BetDouble: 64,
        BetDoubleRes: 65,
        BetGiveUp: 66,
        BetGiveUpRes: 67,
        Leave: 68,
        LeaveRes: 69,
        Error: 999,
      
       
    };

    public static LOCAL = {
        LOCAL_MONEY_CHARGE: 1,
    };

    public static Config = [
        {
            TYPE:"LOCAL",
            HOST: '',
            PORT : -1,
            isSSL : false
        },{
            TYPE: "SCRATCH",
            HOST: "192.168.51.59",
            PORT : 18899,
            isSSL : false
        }
    ];
    static money: any;
    static token: any;
    static currency: any;
    static currencyRate: any;
    static gameName: any;
    static isSSL : any;
    static from = 2;
    
}

// export NT;